
$(document).ready(function(){
    $('.dataTables').dataTable();
});

function callModal( url ) {
	$.ajax({
            type: "POST",
            url: url,
            success: function(result)
                    {
                       	$('.modal-content').html(result);
                    }
                    
        }); //ajax close
}

function setData( url, formName)
    {

    	var form = formName;
    	var data = $('form[name="'+form+'"]').serialize();
        $.ajax({
                    type: "POST",
                    url: url,
                    data:data,
                    success: function(result)
                            {                       
                                $('#myModal').modal('hide');
                                $(".reload").load(" .reload",function(){
                                	$('.dataTables').dataTable();
                                });//Refresh Only Div
                                
                            }
                    
                }); //ajax close
    }

