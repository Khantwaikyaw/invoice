<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	private $path = "Report/"; // Set Path for View
	
	public function currency( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			$data['pages'] = 'Report_Currency_New';
			$this->load->view('admin/pages/Report/index.php', $data );
		}
		else
		{
			$data['pages'] =  $this->path.'/Report_Currency_List';
			$this->load->view('admin/index' , $data);
		}
	}

	public function invoice( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			$data['pages'] = 'Report_Invoice_New';
			$this->load->view('admin/pages/Report/index.php', $data );
		}
		else
		{
			$data['pages'] =  $this->path.'/Report_Invoice_List';
			$this->load->view('admin/index' , $data);
		}
	}

	public function payment( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			$data['pages'] = 'Report_Payment_New';
			$this->load->view('admin/pages/Report/index.php', $data );
		}
		else
		{
			$data['pages'] =  $this->path.'/Report_Payment_List';
			$this->load->view('admin/index' , $data);
		}
	}

}
