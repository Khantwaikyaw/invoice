<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

	private $path = "Content/"; // Set Path for View
	
	function __construct()
	{
		parent::__construct();
			$this->load->model('Article');
				$this->load->model('EmailTemplate');
	}

	public function article( $para1 = '')
	{
		$article = new Article();

		if ( $para1 == 'new')
		{
			$data['pages'] = 'Content_Article_New';
			$this->load->view('admin/pages/Content/index.php', $data );
		}
		elseif ( $para1 == 'add')
		{
			$article->setData( 		$this->input->post('title'),
									$this->input->post('description')
								);
		}
		else
		{
			$data['articleData'] = $article->getData();

			$data['pages'] =  $this->path.'/Content_Article_List';
			$this->load->view('admin/index' , $data);
		}
	}

	public function emailTemplate( $para1 = '')
	{
		$emailTemplate = new EmailTemplate();

		if ( $para1 == 'new')
		{
			$data['pages'] = 'Content_EmailTemplates_New';
			$this->load->view('admin/pages/Content/index.php', $data );
		}
		elseif ( $para1 == 'add' )
		{
			$emailTemplate->setData(	$this->input->post('type'),
										$this->input->post('subject'),
										$this->input->post('message')
									);
		}
		else
		{
			$data['emailTemplateData'] = $emailTemplate->getData();
			$data['pages'] =  $this->path.'/Content_EmailTemplates_List';
			$this->load->view('admin/index' , $data );
		}
	}
	
}
