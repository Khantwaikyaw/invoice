<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_Controller {

	private $path = "Billing/"; // Set Path for View

	function __construct()
	{
		parent::__construct();
			$this->load->model('Customer');
	}

	public function customer( $para1 = '')
	{	
		
		$customer = new Customer();

		if ( $para1 == 'new')
		{
			$data['pages'] = 'Billing_Customer_New';
			$this->load->view('admin/pages/Billing/index.php', $data );
		}
		elseif ( $para1 == 'add')
		{
		
			$customer->setData( $this->input->post('name'),
								$this->input->post('username'),
								$this->input->post('password'),
								$this->input->post('email'),
								$this->input->post('phone'),
								$this->input->post('address')
								);
		
		}
		else
		{
			$data['customerData'] = $customer->getData();
			$data['pages'] =  $this->path.'Billing_Customer_List';
			$this->load->view('admin/index' , $data);
		}
	}

	public function invoice( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			$data['pages'] = 'Billing_Invoice_New';
			$this->load->view('admin/pages/Billing/index.php', $data );
		}
		else
		{
			$data['pages'] =  $this->path.'Billing_Invoice_List';
			$this->load->view('admin/index' , $data);
		}
	}

	public function payment( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			$data['pages'] = 'Billing_RecurringPayment_New';
			$this->load->view('admin/pages/Billing/index.php', $data );
		}
		else
		{
			$data['pages'] =  $this->path.'Billing_RecurringPayment_List';
			$this->load->view('admin/index' , $data);
		}
	}

	
}
