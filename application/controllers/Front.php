<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['pages'] = 'home';
		$this->load->view( 'user/index', $data );
	}

	public function about()
	{
		$data['pages'] = 'about';
		$this->load->view('user/index', $data);
	}
	
	public function contact()
	{
		$data['pages'] = 'contact';
		$this->load->view('user/index', $data);
	}

	public function payment()
	{
		$data['pages'] = 'payment_HowToPay';
		$this->load->view('user/index', $data);
	}
}
