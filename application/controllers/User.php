<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	private $path = "User/"; // Set Path for View
	
	public function users( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			$data['pages'] = 'User_New';
			$this->load->view('admin/pages/User/index.php', $data );
		}
		else
		{
			$data['pages'] =  $this->path.'/User_List';
			$this->load->view('admin/index' , $data);
		}
	}

	

}
