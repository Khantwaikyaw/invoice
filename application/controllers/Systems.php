<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Systems extends CI_Controller {

	private $path = "System/"; // Set Path for View
	
	public function logError( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			//
		}
		else
		{
			$data['pages'] =  $this->path.'/System_Log_error';
			$this->load->view('admin/index' , $data);
		}
	}

	public function logLogin( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			//
		}
		else
		{
			$data['pages'] =  $this->path.'/System_Log_Login';
			$this->load->view('admin/index' , $data);
		}
	}

	public function setting( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			//
		}
		else
		{
			$data['pages'] =  $this->path.'/System_Setting';
			$this->load->view('admin/index' , $data);
		}
	}

	public function status( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			//
		}
		else
		{
			$data['pages'] =  $this->path.'/System_Status';
			$this->load->view('admin/index' , $data);
		}
	}
	

}
