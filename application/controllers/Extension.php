<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extension extends CI_Controller {

	private $path = "Extension/"; // Set Path for View
	
	public function module( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			//here
		}
		else
		{
			$data['pages'] =  $this->path.'/Extension_Module_List';
			$this->load->view('admin/index' , $data);
		}
	}

	public function payment( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			//here
		}
		else
		{
			$data['pages'] =  $this->path.'/Extension_Payment_List';
			$this->load->view('admin/index' , $data);
		}
	}

	public function total( $para1 = '')
	{
		if ( $para1 == 'new')
		{
			//here
		}
		else
		{
			$data['pages'] =  $this->path.'/Extension_Total_List';
			$this->load->view('admin/index' , $data);
		}
	}

}
