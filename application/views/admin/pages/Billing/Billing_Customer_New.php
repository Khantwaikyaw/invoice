<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"> CUSTOMER </h4>
                </div>
 <div class="modal-body">
                                    
<div class="row">
	<div class="form-group">
		<h3 class="text-center"> New Customer </h3> 
	</div>
	
           	<div class="col-md-12"> 
              	<form class="form-horizontal" name="customer" role="form">                                    
	               	<div class="form-group">
	                    <label class="col-md-4 control-label"> Name </label>
	                       	<div class="col-md-7">
	                           	<input name="name" type="text" class="form-control">
	                       	</div>
	               	</div>
	               	<div class="form-group">
	                    <label class="col-md-4 control-label"> Username </label>
	                        <div class="col-md-7">
	                            <input name="username" type="text" class="form-control" >
	                        </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-md-4 control-label"> Password </label>
	                        <div class="col-md-7">
	                            <input name="password" type="password" class="form-control">
	                        </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-md-4 control-label"> E-mail </label>
	                        <div class="col-md-7">
	                            <input name="email" type="password" class="form-control">
	                        </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-md-4 control-label"> Phone </label>
	                        <div class="col-md-7">
	                            <input name="phone" type="text" class="form-control" >
	                        </div>
	                </div>

	                <div class="form-group">
	                    <label class="col-md-4 control-label"> Address </label>
	                        <div class="col-md-7">
	                            <textarea name="address" class="form-control" ></textarea>
	                        </div>
	                </div>

	                <div class="form-group">
	                    <label class="col-md-4 control-label">  </label>
	                        <div class="col-md-7">
	                                <button type="button" class="btn btn-primary fa fa-save" onclick="setData('<?php echo base_url('billing/customer/add'); ?>','customer')"> Save </button>
	                                <button type="button" class="btn btn-danger fa fa-save" data-dismiss="modal"> Cancel </button>
	                        </div>
	                </div>   
	             </form>  
		    </div>   
		
</div>
</div>
<script src="<?php echo base_url('assets/custom/admin/js/method.js'); ?>"></script>
