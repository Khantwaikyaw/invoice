<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs tabs">
            <li class="active tab">
                <a href="#home-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs"> General </span>
                </a>
            </li>
            <li class="tab">
                <a href="#profile-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs"> Websites </span>
                </a>
            </li>
            <li class="tab">
                <a href="#messages-2" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                    <span class="hidden-xs"> Accounting </span>
                </a>
            </li>
            <li class="tab">
                <a href="#settings-2" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-cog"></i></span>
                    <span class="hidden-xs"> Billing </span>
                </a>
            </li>
        </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home-2">
                   <div class="ibox-content">
                        <div class="form-group">
                            <h1 class="text-center"><label class="control-label"> General </label>  </h1>
                        </div>

                        <form class="form-horizontal">
                                <div class="form-group"><label class="col-lg-4 control-label">  Name </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: Mr. Smith" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> E-mail </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: smith212@hotmail.com" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> Phone </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: 09 - *******" class="form-control"></div>
                                </div>
                                                    

                                <div class="form-group"><label class="col-lg-4 control-label"> Address </label>

                                <div class="col-lg-6"><input type="text" placeholder="eg: admin" class="form-control"></div>
                                 </div>

                                <div class="form-group"><label class="col-lg-4 control-label">  </label>

                                <div class="col-lg-6"><input type="submit" value="ADD" class="col-md-4 btn btn-info"></div>
                                 </div>
                        </form>
                    </div>

                    
                </div>
                <div class="tab-pane" id="profile-2">
                   <div class="ibox-content">
                        <div class="form-group">
                            <h1 class="text-center"><label class="control-label"> General </label>  </h1>
                        </div>

                        <form class="form-horizontal">
                                <div class="form-group"><label class="col-lg-4 control-label">  Name </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: Mr. Smith" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> E-mail </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: smith212@hotmail.com" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> Phone </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: 09 - *******" class="form-control"></div>
                                </div>
                                                    

                                <div class="form-group"><label class="col-lg-4 control-label"> Address </label>

                                <div class="col-lg-6"><input type="text" placeholder="eg: admin" class="form-control"></div>
                                 </div>

                                <div class="form-group"><label class="col-lg-4 control-label">  </label>

                                <div class="col-lg-6"><input type="submit" value="ADD" class="col-md-4 btn btn-info"></div>
                                 </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="messages-2">
                   <div class="ibox-content">
                        <div class="form-group">
                            <h1 class="text-center"><label class="control-label"> General </label>  </h1>
                        </div>

                        <form class="form-horizontal">
                                <div class="form-group"><label class="col-lg-4 control-label">  Name </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: Mr. Smith" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> E-mail </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: smith212@hotmail.com" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> Phone </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: 09 - *******" class="form-control"></div>
                                </div>
                                                    

                                <div class="form-group"><label class="col-lg-4 control-label"> Address </label>

                                <div class="col-lg-6"><input type="text" placeholder="eg: admin" class="form-control"></div>
                                 </div>

                                <div class="form-group"><label class="col-lg-4 control-label">  </label>

                                <div class="col-lg-6"><input type="submit" value="ADD" class="col-md-4 btn btn-info"></div>
                                 </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="settings-2">
                    <div class="ibox-content">
                        <div class="form-group">
                            <h1 class="text-center"><label class="control-label"> Billing </label>  </h1>
                        </div>

                        <form class="form-horizontal">
                                <div class="form-group"><label class="col-lg-4 control-label">  Name </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: Mr. Smith" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> E-mail </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: smith212@hotmail.com" class="form-control"></div>
                                </div>
                                <div class="form-group"><label class="col-lg-4 control-label"> Phone </label>

                                <div class="col-lg-6"><input type="email" placeholder="eg: 09 - *******" class="form-control"></div>
                                </div>
                                                    

                                <div class="form-group"><label class="col-lg-4 control-label"> Address </label>

                                <div class="col-lg-6"><input type="text" placeholder="eg: admin" class="form-control"></div>
                                 </div>

                                <div class="form-group"><label class="col-lg-4 control-label">  </label>

                                <div class="col-lg-6"><input type="submit" value="ADD" class="col-md-4 btn btn-info"></div>
                                 </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
                    
</div>