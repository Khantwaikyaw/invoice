<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"> ARTICLE </h4>
                </div>
 <div class="modal-body">
                                    
<div class="row">
	<div class="form-group">
		<h3 class="text-center"> New Article </h3> 
	</div>
	
           	<div class="col-md-12"> 
              	<form class="form-horizontal" name="article" role="form">                                    
	               	<div class="form-group">
	                    <label class="col-md-4 control-label"> Title </label>
	                       	<div class="col-md-7">
	                           	<input name="title" type="text" class="form-control">
	                       	</div>
	               	</div>
	               	<div class="form-group">
	                    <label class="col-md-4 control-label"> Description </label>
	                        <div class="col-md-7">
	                            <textarea name="description" class="form-control" ></textarea>
	                        </div>
	                </div>

	                <div class="form-group">
	                    <label class="col-md-4 control-label">  </label>
	                        <div class="col-md-7">
	                                <button type="button" class="btn btn-primary fa fa-save" onclick="setData('<?php echo base_url('content/article/add'); ?>','article')"> Save </button>
	                                <button type="button" class="btn btn-danger fa fa-save" data-dismiss="modal"> Cancel </button>
	                        </div>
	                </div>   
	             </form>  
		    </div>   
		
</div>
</div>

