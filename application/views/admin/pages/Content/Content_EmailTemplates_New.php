<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"> E-MAIL TEMPLATE </h4>
                </div>
 <div class="modal-body">
                                    
<div class="row">
	<div class="form-group">
		<h3 class="text-center"> New E-mail Template </h3> 
	</div>
	
           	<div class="col-md-12"> 
              	<form class="form-horizontal" name="emailTemplate" role="form">                                    
	               	<div class="form-group">
	                    <label class="col-md-4 control-label"> Type </label>
	                       	<div class="col-md-7">
	                           	<input name="type" type="text" class="form-control">
	                       	</div>
	               	</div>
	               	<div class="form-group">
	                    <label class="col-md-4 control-label"> Subject </label>
	                        <div class="col-md-7">
	                            <input name="subject" type="text" class="form-control" >
	                        </div>
	                </div>
	                <div class="form-group">
	                    <label class="col-md-4 control-label"> Message </label>
	                        <div class="col-md-7">
	                            <textarea name="message" class="form-control" ></textarea>
	                        </div>
	                </div>
	         

	                <div class="form-group">
	                    <label class="col-md-4 control-label">  </label>
	                        <div class="col-md-7">
	                                <button type="button" class="btn btn-primary fa fa-save" onclick="setData('<?php echo base_url('content/emailTemplate/add'); ?>','emailTemplate')"> Save </button>
	                                <button type="button" class="btn btn-danger fa fa-save" data-dismiss="modal"> Cancel </button>
	                        </div>
	                </div>   
	             </form>  
		    </div>   
		
</div>
</div>

