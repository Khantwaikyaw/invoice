<div class="container">
         <div class="row line">
             <div class="form-group col-md-10">
                <div><h2> ARTICLE </h2></div>
             </div>
              <div class="form-group col-md-2">
                <div class="btn-line"><button onclick="callModal('<?php echo base_url('Content/article/new'); ?>')" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> ADD </button></div>
             </div>
            
        </div>
  
</div>  
  <div class="ibox-content reload">

     <table class="table table-striped table-bordered  dataTables" >
        <thead>
            <tr>
                <th>    Title           </th>
                <th>    Description     </th>
                <th>    Status          </th>
         
                
            </tr>
        </thead>
        <tbody>   
        
            <?php foreach ($articleData->result() as $row): ?> 
                <tr>
                    <td>    <?php  echo $row->title; ?>          </td>
                    <td>    <?php  echo $row->description; ?>      </td>
                    <td>    <?php  echo "something"; ?>         </td>
                </tr>
            <?php endforeach ?>
            
        </tbody>
       
    </table>


</div>

<?php include ('/../../templates/modal.php'); ?>