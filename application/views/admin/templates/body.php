<body>

<div id="wrapper">

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                             </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>

            <li>
                <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label"> Dashboard </span> </a>
               
            </li>

             <li>
                <a href="index.html"><i class="fa fa-file-text-o"></i> <span class="nav-label"> Billing </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('Billing/customer'); ?>"> Customers </a></li>
                    <li><a href="<?php echo base_url('Billing/invoice'); ?>"> Invoices </a></li>
                    <li><a href="<?php echo base_url('Billing/payment'); ?>"> Recurring Payment</a></li>     
                </ul>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-list-alt"></i> <span class="nav-label"> Content </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('Content/article'); ?>"> Articles </a></li>
                    <li><a href="<?php echo base_url('Content/emailTemplate'); ?>"> E-mail Template </a></li>
                </ul>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-plus-square-o"></i> <span class="nav-label"> Extensions </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('Extension/module'); ?>"> Modules </a></li>
                    <li><a href="<?php echo base_url('Extension/payment'); ?>"> Payments </a></li>
                    <li><a href="<?php echo base_url('Extension/total'); ?>"> Totals </a></li>
                </ul>
            </li>
             <li>
                <a href="index.html"><i class="fa fa-file-text"></i> <span class="nav-label"> Reports </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('Report/currency'); ?>"> Currency </a></li>
                    <li><a href="<?php echo base_url('Report/invoice'); ?>"> Invoice </a></li>
                    <li><a href="<?php echo base_url('Report/payment'); ?>"> Recurring Payment </a></li>
                </ul>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-gear"></i> <span class="nav-label"> System </span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label"> Log </span> <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li><a href="<?php echo base_url('Systems/logError'); ?>"> Error </a></li>
                            <li><a href="<?php echo base_url('systems/logLogin'); ?>"> Login </a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo base_url('systems/setting'); ?>"> Setting </a></li>
                    <li><a href="<?php echo base_url('Systems/status'); ?>"> Status </a></li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url('user/users'); ?>"><i class="fa fa-user"></i> <span class="nav-label"> User </span> </a>
                
            </li>
            
            <li>
                <a href="css_animation.html"><i class="fa fa-sign-out"></i> <span class="nav-label"> Logout </span></a>
            </li>
           
        </ul>

    </div>
</nav>

<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
          
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <span class="m-r-sm text-muted welcome-message">Welcome to Something</span>
            </li>
            
            


            <li>
                <a href="login.html">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>

    </nav>
</div>



