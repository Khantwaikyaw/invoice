<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include ('templates/head.php');
include ('templates/body.php');
include ('pages/'.$pages.'.php');
include ('templates/foot.php');
