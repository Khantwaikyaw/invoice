<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailTemplate extends CRUD{

	protected $CI; //to use CI supported function

	private $table = 'emailtemplate';

	
	function __construct()
	{
		$this->CI =& get_instance();
	}

	public function setData( $type, $subject, $message )
	{
		$data = array ( 'type'		=>	$type,
						'subject'	=>	$subject,
						'message'	=>	$message
						);

		$this->create( $this->table, $data);
	}

	public function getData()
	{
		return $this->read( $this->table );
	}
	
	
}


