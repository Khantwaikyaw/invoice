<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CRUD{

	protected $CI; //to use CI supported function

	private $table = 'customer';

	
	function __construct()
	{
		$this->CI =& get_instance();
	}

	public function setData( $name, $username, $password, $email, $phone, $address )
	{
		$data = array ( 'name'			=>	$name,
						'username'		=>	$username,
						'password'		=>	$password,
						'hashpassword'	=>	sha1($password),	
						'email'			=>	$email,
						'phone'			=>	$phone,
						'address'		=>	$address
						);

		$this->create( $this->table, $data);
	}

	public function getData()
	{
		return $this->read( $this->table );
	}
	
	
}


