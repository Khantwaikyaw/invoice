<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CRUD extends CI_Model {

	
	public function create( $table, $data)
	{
		$this->db->insert( $table, $data );
	}

	public function read( $table )
	{
		return $this->db->get( $table );
	}

	// public function Update( $table, $data, $db_field_id, $id)
	// {
	// 	$this->db->where($db_field_id, $id);
	// 	$this->db->update($table, $data);
	// }

	// public function Delete()
	// {
	// 	echo "hello";
	// }

	// public function readSpecific( $table, $data )
	// {
	// 	return $this->db->get_where( $table, $data );
	// }
	
	
}
