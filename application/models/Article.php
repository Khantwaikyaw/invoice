<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CRUD{

	protected $CI; //to use CI supported function

	private $table = 'article';

	
	function __construct()
	{
		$this->CI =& get_instance();
	}

	public function setData( $title, $description )
	{
		$data = array ( 'title'			=>	$title,
						'description'	=>	$description
						);

		$this->create( $this->table, $data);
	}

	public function getData()
	{
		return $this->read( $this->table );
	}
	
	
}


